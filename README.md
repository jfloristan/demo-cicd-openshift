## Demo deploying from S2I
--------------------------

### Prerequisites

   - Jenkins available in the cluster with edit permissions
   ```
   $ oc adm policy add-cluster-role-to-user edit system:serviceaccount:<jenkins namespace>:jenkins  
   ```
   
   - Admin access to the cluster
   - One developer user

### Prepare environment AS ADMIN USER

  Define ENV
  ```
  $ export DEVELOPER=user1
  $ export DEV_PROJECT=cicd-demo-dev
  $ export STAGE_PROJECT=cicd-demo-stage
  $ export JENKINS_PROJECT=<jenkins namespace>
  ```

  Create projects
  ```
  $ oc new-project $DEV_PROJECT
  $ oc new-project $STAGE_PROJECT
  ```
  
  Add developer permission to view dev and stage projects
  ```
  $ oc adm policy add-role-to-user view $DEVELOPER -n $DEV_PROJECT
  $ oc adm policy add-role-to-user view $DEVELOPER -n $STAGE_PROJECT
  $ oc adm policy add-role-to-user view $DEVELOPER -n $JENKINS_PROJECT
  ```
  
### Create the app AS DEVELOPER USER

  Define ENV
  ```
  $ export APP=cicd-demo
  $ export DIR=app
  $ export PROJECT=cicd-demo-user
  ```
  
  Create project
  ```  
  $ oc new-project $PROJECT
  ```
  
  Create App
  ```
  $ oc new-app $DIR --name=$APP
  ```
  
  Build App
  ```
  $ oc start-build $APP --from-dir $DIR --follow
  ```

  Expose Service
  ```
  $ oc expose service $APP
  ```
  
### Clean application

  ```
  $ oc delete all --selector app=$APP -o name
  $ oc delete project $PROJECT
  ```
  

