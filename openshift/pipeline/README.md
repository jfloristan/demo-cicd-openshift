This directory contains a Jenkinsfile which can be used to build
demo-cicd using an OpenShift build pipeline.

To do this, run:

```bash
# create the nodejs example as usual
oc new-app https://gitlab.com/jfloristan/demo-cicd-openshift

# now create the pipeline build controller from the openshift/pipeline
# subdirectory
oc new-app  https://gitlab.com/jfloristan/demo-cicd-openshift \
  --context-dir=openshift/pipeline --name demo-cicd-pipeline
```
