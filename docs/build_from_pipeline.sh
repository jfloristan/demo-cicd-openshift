export APP=corporate-web

oc new-app https://github.com/sclorg/nodejs-ex --context-dir=openshift/pipeline --name=$APP

oc expose service $APP
